
package br.com.senac.test;

import br.com.senac.ex5.Numeros;
import org.junit.Test;
import static org.junit.Assert.*;


public class NumerosTeste {
    
    public NumerosTeste() {
        
        
    }
    
    @Test
    public void numerosDeveSerImpar(){
        Numeros numeros = new Numeros();
        int numero = 3;
        boolean resultado = numeros.isImpar(numero);
        
        assertTrue(resultado);
    }
    @Test
    public void numerosNaoDeveSerImpar(){
        Numeros numeros = new Numeros();
        int numero = 2;
        boolean resultado = numeros.isImpar(numero);
        assertFalse(resultado);
    }
    @Test
    public void numerosDeveSerPar(){
        Numeros numeros = new Numeros();
        int numero = 2;
        boolean resultado = numeros.isPar(numero);
        assertTrue(resultado);
    }
    @Test
    public void numerosNaoDeveSerPar(){
        Numeros numeros = new Numeros();
        int numero = 3;
        boolean resultado = numeros.isPar(numero);
        assertFalse(resultado);
    }
    
    @Test
    public void numerosDeveSerPositivo(){
        Numeros numeros = new Numeros();
        int numero = 2;
        boolean resultado = numeros.isPositivo(numero);
        assertTrue(resultado);
    }
    @Test
    public void numerosNaoDeveSerPositivo(){
        Numeros numeros = new Numeros();
        int numero = -2;
        boolean resultado = numeros.isPositivo(numero);
        assertFalse(resultado);
    }
    
    @Test
    public void numerosDeveSerNegativo(){
        Numeros numeros = new Numeros();
        int numero = -2;
        boolean resultado = numeros.isNegativo(numero);
        assertTrue(resultado);
    }
    
    @Test
    public void numerosNaoDeveSerNegativo(){
        Numeros numeros = new Numeros();
        int numero = 2;
        boolean resultado = numeros.isNegativo(numero);
        assertFalse(resultado);
    }
    
}
