
package br.com.senac.ex5;


public class Numeros {
    
    
    private int numeros;

    public boolean isImpar(int numero) {
        return numero % 2 == 1;
    }

    public boolean isPar(int numero) {
        return numero % 2 == 0;
    }

    public boolean isNegativo(int numero) {
        return numero < 0;
    }

    public boolean isPositivo(int numero) {
        return numero > 0;
    }
    
   
    
}
